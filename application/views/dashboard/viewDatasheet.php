<?php $this->load->view('dashboard/common/header.php')?>

<?php $this->load->view('dashboard/common/sidebar.php')?>
        
<div class="content-wrapper p-3">
    <?php $this->load->view('dashboard/sections/error') ?>
 
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Datasheet Information</h3>
                </div>
                <div class="card-body"> 
                    <div class="row">
                        <div class="col-lg-4">
                            <p>
                                <b>Datasheet</b><br />
                                <?php $datasheet = explode("/", $datasheetInfo->datasheet); echo $datasheet[2]; ?>
                            </p>
                        </div>
                        <div class="col-lg-3">
                            <p>
                                <b>Number of B/L Imported</b><br />
                                <?php echo count($bls); ?>
                            </p>
                        </div>
                        <div class="col-lg-3">
                            <p>
                                <b>Uploaded at</b><br />
                                <?php echo $datasheetInfo->updatedOn; ?>
                            </p>
                        </div>

                        <div class="col-lg-2">
                            <a class="btn btn-md btn-danger" onclick="return confirm('Do you want to delete the datasheet?')" href="<?php echo base_url('admin/datasheet/delete/');?><?php echo $datasheetInfo->id ?>">Delete All Data</a>
                        </div>
                        
                        <div class="col-lg-12 pt-5">
                            <h3>List of B/L from datasheet</h3>

                            <?php foreach($bls as $bl): ?>
                                <div class="row blBox">
                                    <div class="col-lg-3">
                                        <b>BDI Item No</b><br />
                                        <?php echo $bl->bdiItemNo; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>BL Number</b><br />
                                        <?php echo $bl->blNumber; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>BLC Container</b><br />
                                        <?php echo $bl->blcContainer; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>BLC Conttype</b><br />
                                        <?php echo $bl->blcConttype; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>Customer Code</b><br />
                                        <?php echo $bl->customerCode; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>Customer Name</b><br />
                                        <?php echo $bl->customerName; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>Vessel Name</b><br />
                                        <?php echo $bl->vesselName; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>BL Voyage</b><br />
                                        <?php echo $bl->blVoyage; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>Voyage Eta</b><br />
                                        <?php echo $bl->voyageEta; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>BLC Return Depot</b><br />
                                        <?php echo $bl->blcReturnDepot; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>Customer Phone</b><br />
                                        <?php echo $bl->customerPhone; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>Customer Email</b><br />
                                        <?php echo $bl->customerEmail; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>BLC Short Description</b><br />
                                        <?php echo $bl->blcShortDescription; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>Free Days</b><br />
                                        <?php echo $bl->freeDays; ?>
                                    </div>
                                    <div class="col-lg-3">
                                        <b>FCL Validity</b><br />
                                        <?php echo $bl->fclValidity; ?>
                                    </div>
                                    <div class="col-lg-3 uniqueCode">
                                        <b>Unique Code</b><br />
                                        <?php echo $bl->uniqueCode; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
    </section>
</div> 

<?php $this->load->view('dashboard/common/footer.php')?>