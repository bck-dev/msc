<?php $this->load->view('dashboard/common/header.php')?>

<?php $this->load->view('dashboard/common/sidebar.php')?>
        
<div class="content-wrapper p-3">
    <?php $this->load->view('dashboard/sections/error') ?>
 
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Datasheet</h3>
                </div>
                <div class="card-body"> 
                    <form action="<?php echo base_url('admin/datasheet/upload');  ?><?php echo $data->id ?>" method="POST" name="addForm" enctype="multipart/form-data" >
                        <div class="row">
                            <div class="form-group col-lg-10">
                                <label for="datasheet">Select Datasheet</label>
                                <br />
                                <input type="file" name="datasheet" id="datasheet" class="form-control" required/>
                            </div>
                            <div class="form-group col-lg-2">
                                <label></label>
                                <button type="submit" class="btn btn-primary btn-lg btn-block" name="submit">Upload</button>
                            </div>
                        </div>
                    </form>

                    
                </div>
            </div>
        </div>
    </section>
</div> 

<?php $this->load->view('dashboard/common/footer.php')?>