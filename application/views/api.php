<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>MSC | API check</title>

    <script src="<?php echo base_url() ?>assets/adminLte/plugins/jquery/jquery.min.js"></script></head>

<body>
    <h1>API Checker</h1>
    <button id="check">Check</button>
</body>

<script>
$(document).ready(function(){

    $('#check').click(function(){

        $.ajax({
            type:'GET',
            url: '<?php echo base_url('api/bank/validate') ?>', 
            data: {code: "1"},
            dataType: 'json',
            success: function(results){ 
                console.log(results);
            },
        
            error:function(){
                console.log('error');
            }
        });
    });
 
});
</script>

</html>