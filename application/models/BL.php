<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class BL extends CI_Model
{

    function clearAndFormat($string, $type=null){
        $length=strlen($string);
        $last = $length-1; 
        if($string[$last]==" "){
            $string = substr($string, 0, -1); 
        }
        
        if($string[0]==" "){
            $string = substr($string, 1);
        }

        if($type=="date"){
            $array = explode("/",$string);
            if($array[2][0]==0){
                $array[2][0]=2;
            }

            $string=$array[2].'-'.$array[1].'-'.$array[0];
        }
        
        return $string;
    }

    function generateUniqueCode(){
        
        $random = rand(1000, 9999);
        if($this->checkUniqueCodeExsistance($random)==1){
            return $this->generateUniqueCode();
        }else{
            return $random;
        }
    }

    public function checkUniqueCodeExsistance($code) {
        $this->db->select('id');
        $this->db->from('bl');
        $this->db->where('uniqueCode', $code); 
        $this->db->where('activeStatus', 1);   
        return $this->db->count_all_results();
    }

    public function listAllCustomers(){
        $this->db->select('customerCode');
        $this->db->distinct('customerCode');
        $this->db->order_by('customerName');
        $customers = $this->db->get('bl')->result();
        
        foreach ($customers as $cus){
            $cus->customerName= $this->getCustomerName($cus->customerCode);
        }

        return $customers;
    }

    public function getCustomerName($customerCode){
        $this->db->select('customerName');
        $this->db->from('bl');
        $this->db->where('customerCode', $customerCode); 
        $this->db->limit(1);
        
        $customer = $this->db->get()->row();

        return $customer->customerName;
    }
}
?>