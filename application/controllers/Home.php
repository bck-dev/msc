<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Home extends BaseController {

	public function __construct()
  {
      parent::__construct();
      $this->isLoggedIn();   
  }

  public function index()
  {
    $this->load->view('dashboard/dashboard');
  }

  public function checkAPI()
  {
    $this->load->view('api');
  }

}