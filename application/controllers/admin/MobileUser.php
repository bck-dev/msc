<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class MobileUser extends BaseController {

  public function __construct()
  {
      parent::__construct();
      $this->isLoggedIn();   
  }

  public function index()
  {

    $mobileUsers = $this->Common->getAllData('mobile_users');
    
    $tableData=['pageName'=>"Mobile Users",
            'mobileUsers' => $mobileUsers, 
            'action'  => 'adduser',
            'customers' => $this->BL->listAllCustomers()
            ];

    $this->load->view('dashboard/mobileUser', $tableData);
  }

  public function addUser()
  {
    $hashPassword = password_hash($_POST['password'], PASSWORD_BCRYPT );

    $data = array(
        'fullName' => $_POST['name'],
        'password' => $hashPassword,
        'email' =>$_POST['email'],
        'phone' => $_POST['phone'],
        'customerCode' => $_POST['customerCode'],
        'customerName' => $this->BL->getCustomerName($_POST['customerCode'])
    );  

    $this->db->insert('mobile_users', $data);

    $mobileUsers = $this->Common->getAllData('mobile_users');

    $tableData=['pageName'=>"Mobile Users",
    'mobileUsers' => $mobileUsers ,
    'check' => 'success',
    'action'  => 'adduser',
    'customers' => $this->BL->listAllCustomers()
    ];

    $this->load->view('dashboard/mobileUser', $tableData);

  }

  public function delete($id){

    $this->Common->delete('mobile_users', $id);
    $mobileUsers = $this->Common->getAllData('mobile_users');
    $data=['pageName'=>"Mobile Users",
            'mobileUsers' => $mobileUsers,
            'action'  => 'adduser',
            'customers' => $this->BL->listAllCustomers()
        ];
    $this->load->view('dashboard/mobileUser', $data); 

  }

  public function loadUpdate($id){

    $updateUser = $this->Common->getById('mobile_users',$id);

    $mobileUsers = $this->Common->getAllData('mobile_users');

    $tableData=['pageName'=>"Mobile Users",
                'mobileUsers' => $mobileUsers ,
                'data'  => $updateUser,
                'action'  => 'update',
                'customers' => $this->BL->listAllCustomers()
              ];

    $this->load->view('dashboard/mobileUser', $tableData);

  }

  public function update($id){

    if(!$_POST['password']):
    $data = array(
      'fullName' => $_POST['name'],
      'email' =>$_POST['email'],
      'phone' => $_POST['phone'],
      'customerCode' => $_POST['customerCode']
    ); 
    else:
    $hashPassword = password_hash($_POST['password'], PASSWORD_BCRYPT );
    $data = array(
      'fullName' => $_POST['name'],
      'password' => $hashPassword,
      'email' =>$_POST['email'],
      'phone' => $_POST['phone'],
      'customerCode' => $_POST['customerCode']
    ); 

    endif;

    $this->Common->update('mobile_users', $id, $data);
    $mobileUsers = $this->Common->getAllData('mobile_users');

    $tableData=['pageName'=>"Mobile Users",
                'mobileUsers' => $mobileUsers ,
                'action'  => 'addUser',
                'customers' => $this->BL->listAllCustomers()
              ];

    $this->load->view('dashboard/mobileUser', $tableData);
    
  }
}

?>