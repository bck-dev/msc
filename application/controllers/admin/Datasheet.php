<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Datasheet extends BaseController {

  public function __construct()
  {
      parent::__construct();
      $this->isLoggedIn();   
  }

  public function index()
  {
    $data=['pageName'=>'Datasheet'];

    $this->load->view('dashboard/datasheet', $data);
  }

  public function uploadDatasheet(){
		$datasheet = $this->Common->upload('datasheet');

		$data = array(
      'datasheet' => $datasheet
		);

    $this->db->insert('datasheet', $data);
    $datasheetId = $this->db->insert_id();

    $handle = fopen($datasheet,"r");
    $i=0;
    while (($row = fgetcsv($handle, 10000, ",")) != FALSE) //get row vales
    {
      if($i!=0){

        $blData=array(
          'bdiItemNo' => $this->BL->clearAndFormat($row[0]),
          'blNumber' => $this->BL->clearAndFormat($row[1]),
          'blcContainer' => $this->BL->clearAndFormat($row[2]),
          'blcConttype' => $this->BL->clearAndFormat($row[3]),
          'customerCode' => $this->BL->clearAndFormat($row[4]),
          'customerName' => $this->BL->clearAndFormat($row[5]),
          'vesselName' => $this->BL->clearAndFormat($row[6]),
          'blVoyage' => $this->BL->clearAndFormat($row[7]),
          'voyageEta' => $this->BL->clearAndFormat($row[8], 'date'),
          'blcReturnDepot' => $this->BL->clearAndFormat($row[9]),
          'customerPhone' => $this->BL->clearAndFormat($row[10]),
          'customerEmail' => $this->BL->clearAndFormat($row[11]),
          'blcShortDescription' => $this->BL->clearAndFormat($row[12]),
          'freeDays' => $this->BL->clearAndFormat($row[13]),
          'fclValidity' => $this->BL->clearAndFormat($row[14], 'date'),
          'uniqueCode' => $this->BL->generateUniqueCode(),
          'datasheetId' => $datasheetId,
          'activeStatus' => 1
        );

        $this->db->insert('bl', $blData);
      }
      $i++;
    }

    redirect ('admin/datasheet/view/'.$datasheetId);
  }
  
  public function view($datasheetId){
    $datasheetInfo = $this->Common->getById('datasheet', $datasheetId);
    $bls = $this->Common->getByFeild('bl', 'datasheetId', $datasheetId);

    $data = array (
      'pageName'=>'Datasheet',
      'datasheetInfo' => $datasheetInfo,
      'bls' => $bls
      
    );

    $this->load->view('dashboard/viewDatasheet', $data);
  }

  public function delete($datasheetId){
    $this->Common->delete('datasheet', $datasheetId);
    $this->Common->deleteByFeild('bl', 'datasheetId', $datasheetId);
    
    $data = array (
      'pageName'=> 'Datasheet'      
    );

    $this->load->view('dashboard/datasheet', $data);
  }

} 

?>