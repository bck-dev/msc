<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php';
require APPPATH . '/libraries/REST_Controller.php';

use \Firebase\BeforeValidException\BeforeValidException;
use \Firebase\ExpiredException\ExpiredException;
use \Firebase\SignatureInvalidException\SignatureInvalidException;
use \Firebase\JWT\JWT;

class UniqueCode extends REST_Controller {

  public function __construct()
  {
    parent::__construct(); 

    $token = $this->input->get_request_header('Authorization');
    echo $token;
    if($token){

      echo 1;
      $tokenParts = explode(" ", $token);
      $jwt = $tokenParts[1];
      $key = $this->input->get_request_header('x-api-key');
      try {
          $decode = JWT::decode($jwt, $key, array('HS256'));
      } catch(Exception $e) {
          $this->response(array(
            "status" => "failed",
            "message" => "Signature verification failed",
            "token" => $jwt
          ), REST_Controller::HTTP_NOT_FOUND);
      }
    }
    else{
      echo 2;
      $key = $this->input->get_request_header('x-api-key');
      if ($this->Common->getCount('api_keys', 'apiKey', $key)>0){
        echo 3;
        $token = array(
          "key" => $key,
          "generatedTime" => strtotime(date("D-M-Y H:i:s"))
        );
      
        $jwt = JWT::encode($token, $key);
      }
      else{
        echo 4;
        $this->response(array(
          "status" => "failed",
          "message" => "Invalid API Key"
        ), REST_Controller::HTTP_NOT_FOUND);
      }
      
    }
  }

  public function index_post()
  {
    $this->form_validation->set_rules('code', "Code", "required|regex_match[/^[0-9]{4}$/]|xss_clean");

    if($this->form_validation->run()===FALSE){
      $this->response(array(
        "error" => "Unique Code is Missing or Invalid Format"
      ), REST_Controller::HTTP_NOT_FOUND);
    }
    else{
      $code = $this->input->post('code');
      
      $this->db->select('id');
      $this->db->from('bl');
      $this->db->where('uniqueCode', $code);
      $this->db->where('activeStatus', 1);

      $count = $this->db->count_all_results();

      if($count>0){
        $this->response(array(
          "code" => $code,
          "validity" => TRUE,
          "token" => $this->jwt
        ), REST_Controller::HTTP_OK);
      }else{
        $this->response(array(
          "code" => $code,
          "validity" => FALSE,
          "token" => $this->jwt
        ), REST_Controller::HTTP_NOT_FOUND);
      }
    }
  }

} 

?>