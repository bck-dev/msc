<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['admin/dashboard'] = 'admin/dashboard';
// datasheet
$route['admin/datasheet'] = 'admin/datasheet';
$route['admin/datasheet/upload'] = 'admin/datasheet/uploadDatasheet';
$route['admin/datasheet/view/(:num)'] = 'admin/datasheet/view/$1';
$route['admin/datasheet/delete/(:num)'] = 'admin/datasheet/delete/$1';

// mobile users
$route['admin/mobileuser'] = 'admin/mobileUser';
$route['admin/mobileuser/adduser'] = 'admin/mobileUser/addUser';
$route['admin/mobileuser/update'] = 'admin/mobileUser/update';
$route['admin/mobileuser/delete/(:num)'] = 'admin/mobileUser/delete/$1';
$route['admin/mobileuser/loadupdate/(:num)'] = 'admin/mobileUser/loadUpdate/$1';

// admin users
$route['admin/user'] = 'admin/user';
$route['admin/user/adduser'] = 'admin/user/addUser';
$route['admin/user/update'] = 'admin/user/update';
$route['admin/user/delete/(:num)'] = 'admin/user/delete/$1';
$route['admin/user/loadupdate/(:num)'] = 'admin/user/loadUpdate/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'admin/login';
$route['admin'] = 'admin/login';
$route['login/check'] = 'admin/login/checkLogin';
$route['logout'] = 'admin/user/logout';

//api call
$route['api'] = 'home/checkAPI';
$route['api/bank/validate'] = 'api/uniqueCode/index_post';
$route['api/bank/decode'] = 'api/uniqueCode/index_get';